/* 
AJAX это паттерн, подразумевающий совершение асинхронных(неблокирующих) запросов на сервер, при которых вся страница не перезагружается при получении ответа, а полученный ответ динамически встраивается в страницу средствами js. При этом в процессе ожидания ответа пользователь может дальше взаимодествойвать со страницей и нет надобности заново загружать всю страницу при получении каких то данных с сервера.
*/

const list = document.createElement('ul');
list.innerHTML = 'There are following "Starwars" episodes:'
document.body.appendChild(list);
const rootUrl = "https://ajax.test-danit.com/api/swapi/films";

const fetchJSON = (url, initRequest) =>
    fetch(url, initRequest)
    .then(response => response.json());

function addHtmlElement(htmlElement, text, appendToElement) {
    const element = document.createElement(htmlElement);
    element.innerHTML = text;
    appendToElement.appendChild(element);
}

function renderEpisodes(episodes) {
    const episodesLinks = [];
    episodes.forEach(({
        episodeId,
        name: title,
        openingCrawl,
        characters
    }) => {
        addHtmlElement('li', `Episode ${episodeId}: ${title}. </br> ${openingCrawl} </br> Characters:`, list);

        episodesLinks.push(characters);
    });

    return episodesLinks;
}

function renderCharacters(episodesLinks) {
    const episodes = document.getElementsByTagName('li');
    episodesLinks.forEach(function (episodeLinks, index) {
        const episode = episodes[index];
        episodeLinks.forEach(episodeLink => {
            fetchJSON(episodeLink)
                .then(({
                    name
                }) => {
                    addHtmlElement('div', name, episode);
                })
        })
    })
}

fetchJSON(rootUrl)
    .then(episodes => {
        return renderEpisodes(episodes);
    })
    .then(episodesLinks => {
        renderCharacters(episodesLinks);
    })